function T = HogTrainner(positiveSamples,negativeSamples,clazzes, minLamda, maxLamda, positiveValidation,negativeValidation )
%READER Summary of this function goes here
%   Detailed explanation goes here

P = double(positiveSamples);
N = double(negativeSamples);

positiveClass  =  clazzes{1,1};
negativeClass =  clazzes{1,2};
C = zeros(size(P,1)+size(N,1),1);

C(1:size(P,1),1) = positiveClass;
C(size(P,1)+1:end,1) = negativeClass;


expectedResultPositiveValidation = repmat(positiveClass,size(positiveValidation,1),1);
expectedResultNegativeValidation = repmat(negativeClass,size(negativeValidation,1),1);

PositiveValidationInput= [positiveValidation ones(size(positiveValidation,1),1)];
NegativeValidationInput = [negativeValidation ones(size(negativeValidation,1),1)];
%minLamda = 10^4;
%maxLamda = 10^10;
X = vertcat(P,N) ;
jump = 10^0.1;
results = zeros(size(X,1),size(X,1));
i  = 1 ;
lamda = minLamda;
while lamda <= maxLamda 
    display('================================');
    display('train by ridge with lamda=');
    display(lamda);
    
W = RidgeTrain(X,C,lamda);
%results(i,1 ) = W;
% todo - check the next line 
%result_X = sign( [X' ones(size(X,1),1)]*W );
result_X = sign( [X ones(size(X,1),1)]*W' );

result_positive_validation =sign( PositiveValidationInput*W' );
result_negative_validation = sign( NegativeValidationInput*W' );

V = Validation(result_X,C);
display('validation train result:');
display(V);

V = Validation(result_positive_validation,expectedResultPositiveValidation);
display('validation positive result:');
display(V);
V = Validation(result_negative_validation,expectedResultPositiveValidation);
display('validation negative result:');
display(V);
%V = Validation(negativeValidation,expectedResultNegativeValidation,1);
lamda = lamda * jump;
i = i + 1 ;
end
T = result_X;
    

end

