%A = Reader('VldN1.pgm');

STR_POSITIVE_IMAGE_FORMAT = 'TrnP%d.pgm';
STR_NEGATIVE_IMAGE_FORMAT = 'TrnN%d.pgm';
STR_TESTING_POSITIVE_IMAGE_FORMAT = 'VldP%d.pgm';

STR_TESTING_NEGATIVE_IMAGE_FORMAT = 'VldN%d.pgm';

INT_NEGATIVE_CLASS = -1;
INT_POSITIVE_CLASS = 1;
FLOAT_MIN_LAMDA = 10^4;
FLOAT_MAX_LAMDA = 10^10;

INT_MAX_TRAIN_IMAGE=300;
INT_MAX_VAILIDATION_IMAGE=150;


%INT_MAX_TRAIN_IMAGE=4;
%INT_MAX_VAILIDATION_IMAGE=4;

h = waitbar(0,'load positive image files');
    positiveImages = loadImages(STR_POSITIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    p = Convert(positiveImages);
close(h);

h = waitbar(0,'load negative image files');
    negativeImages = loadImages(STR_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    n = Convert(negativeImages);    
close(h);


h = waitbar(0,'load *tesintg* negative image files');
    testingNegativeImages = loadImages(STR_TESTING_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_n = Convert(testingNegativeImages);    
close(h);

display('classify the negative images...');
   % negative_clazzes = svmclassify(trained,t_n);
display('done.');

h = waitbar(0,'load *tesintg* positive image files');
    testingNegativeImages = loadImages(STR_TESTING_POSITIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_p = Convert(testingNegativeImages);    
close(h);


display('training by Ridge regression..')
    [errorRate,lambdaParameter, positiveClazz,negativeClazz] = RidgeTrainner(p,n,{INT_POSITIVE_CLASS,INT_NEGATIVE_CLASS},FLOAT_MIN_LAMDA ,FLOAT_MAX_LAMDA,t_p,t_n);
display('done.')

figure;
b = bar(lambdaParameter,errorRate,'FaceColor',[0 .5 .5],'EdgeColor',[0 .9 .9],'LineWidth',1.5);
set(b(1),'FaceColor',[0,1,0]);
set(b(2),'FaceColor',[1,1,0]);
plot(log10(lambdaParameter),errorRate);
title('Lamda vs Error rate');
legend('y = train error rate','y = validation error rate')
xlabel('log(lamda)');
ylabel('Error rates'); % y-axis label
display('classify the positive images...');
 %   positive_clazzes = svmclassify(trained,t_p);
display('done.');

%A = Reader('VldN1.pgm');
