%A = Reader('VldN1.pgm');

STR_POSITIVE_IMAGE_FORMAT = 'TrnP%d.pgm';
STR_NEGATIVE_IMAGE_FORMAT = 'TrnN%d.pgm';
STR_TESTING_POSITIVE_IMAGE_FORMAT = 'VldP%d.pgm';
STR_TESTING_NEGATIVE_IMAGE_FORMAT = 'VldN%d.pgm';
STR_NEGATIVE_CLASS = 'does not have car';
STR_POSITIVE_CLASS = 'hasCar';

INT_MAX_TRAIN_IMAGE=300;
INT_MAX_VAILIDATION_IMAGE=150;


%INT_MAX_TRAIN_IMAGE=4;
%INT_MAX_VAILIDATION_IMAGE=4;

h = waitbar(0,'load positive image files');
    positiveImages = loadImages(STR_POSITIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    p = Convert(IntensityToHog(positiveImages));
close(h);

h = waitbar(0,'load negative image files');
    negativeImages = loadImages(STR_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    n = Convert(IntensityToHog(negativeImages));    
close(h);

display('training by svm...')
trained = Trainer(p,n,{'hasCar','does not have car'});
display('done.')




h = waitbar(0,'load *tesintg* negative image files');
    testingNegativeImages = loadImages(STR_TESTING_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_n = Convert(IntensityToHog(testingNegativeImages));    
close(h);

display('classify the negative images...');
    negative_clazzes = svmclassify(trained,t_n);  
display('done.');


display('calculate the negative classifiy error..')
    validate = negative_clazzes;
    V = Validation(validate,repmat(cellstr(STR_NEGATIVE_CLASS),size(validate,1),1));
display(sprintf('done. negative classifiy error is %f ', V));


h = waitbar(0,'load *tesintg* positive image files');
    testingPositiveImages = loadImages(STR_TESTING_POSITIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_p = Convert(IntensityToHog(testingPositiveImages));    
close(h);

display('classify the positive images...');
    positive_clazzes = svmclassify(trained,t_p);
display('done.');

display('calculate the positive classifiy error..')
    validate = positive_clazzes;
    V = Validation(validate,repmat(cellstr(STR_POSITIVE_CLASS),size(validate,1),1));
display(sprintf('done. positive classifiy error is %f ', V));


%A = Reader('VldN1.pgm');
