function [V_POSITIVE,V_NEGATIVE,T_POSITIVE ,T_NEGATIVE] = AdaConvert( cellsTrainNegative, cellsTrainPositive, cellsTestNegative, cellsTestPositive)
%READER Convert cell to vectors in R^d but instead of just doing it on
%pixel intesity use Hog descriptore to achieve that
%   Detailed explanation goes here

%multi matrix
numberOfTrainingCells = size(cellsTrainPositive,1);
numberOfValidationCells = size(cellsTestPositive,1);


trainedNegativeClazz= -1* ones(numberOfTrainingCells, 1);
trainedPositiveClazz= ones(numberOfTrainingCells, 1);

validationNegativeClazz= ones(numberOfValidationCells, 1);
valdationPositiveClazz= ones(numberOfValidationCells, 1);

clazz = vertcat(trainedNegativeClazz,trainedPositiveClazz);

%allclazz = vertcat(clazz,validationNegativeClazz ,valdationPositiveClazz);

T_POSITIVE = cell(size(cellsTrainNegative{1}));
T_NEGATIVE = cell(size(cellsTrainNegative{1}));
V_POSITIVE = cell(size(cellsTestPositive{1}));
V_NEGATIVE = cell(size(cellsTestPositive{1}));
 
    for i=1:size(cellsTrainNegative{1},1)
        for j=1:size(cellsTrainNegative{1},2)
           display(sprintf('training block (%d,%d) ...', i,j));
           
           B = cell(numberOfTrainingCells * 2,1);
           
           for k=1:numberOfTrainingCells
               B{k} =  cellsTrainNegative{k}{i,j};
           end
           
           for q=1:numberOfTrainingCells
               B{k+q} = cellsTrainPositive{q}{i,j};
           end
           
                     
            trained = svmtrain(Convert(B),clazz);
           
           Test = cell(numberOfValidationCells *2,1);
           for t=1:numberOfValidationCells
              Test {t} =  cellsTestNegative{t}{i,j};
           end
           
           for v=1:numberOfValidationCells
               Test{t+v} = cellsTestPositive{v}{i,j};
           end
           
           T_POSITIVE{i,j} = svmclassify(trained,Convert(B(1:size(B,1)/2))); 
           T_NEGATIVE{i,j} = svmclassify(trained,Convert(B(size(B,1)/2+ 1:end))); 
           
           V_POSITIVE{i,j} = svmclassify(trained,Convert(Test(1:size(Test,1)/2))); 
           V_NEGATIVE{i,j} = svmclassify(trained,Convert(Test(size(Test,1)/2+1:end))); 
           display('done');
        end
    end
    
    
    
%   hogCells{k} = 
%end




%D = reshape(cell2mat(cells), [size(cells,1),4000 ]);


end

