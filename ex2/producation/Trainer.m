function [T,V] = Trainer(positiveSamples,negativeSamples,clazzes )
%READER Summary of this function goes here
%   Detailed explanation goes here

display('training...')
    P = double(positiveSamples);
    N = double(negativeSamples);
    C = cell(size(P,1)+size(N,1),1);
    C(1:size(P,1),1) = cellstr(clazzes{1,1});
    C(size(P,1)+1:end,1) = cellstr(clazzes{1,2});
    T = svmtrain(vertcat(P,N),C);
display('done.')

display('calculate the trainning error..')
    validate = svmclassify(T,vertcat(P,N));
    V = Validation(validate,C);
display(sprintf('done. train error is %f ', V));
end

