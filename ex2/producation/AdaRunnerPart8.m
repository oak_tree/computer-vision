%A = Reader('VldN1.pgm');

STR_POSITIVE_IMAGE_FORMAT = 'TrnP%d.pgm';
STR_NEGATIVE_IMAGE_FORMAT = 'TrnN%d.pgm';
STR_TESTING_POSITIVE_IMAGE_FORMAT = 'VldP%d.pgm';

STR_TESTING_NEGATIVE_IMAGE_FORMAT = 'VldN%d.pgm';

INT_NEGATIVE_CLASS = -1;
INT_POSITIVE_CLASS = 1;


INT_MAX_TRAIN_IMAGE=300;
INT_MAX_VAILIDATION_IMAGE=150;


%INT_MAX_TRAIN_IMAGE=4;
%INT_MAX_VAILIDATION_IMAGE=4;

INT_MAX_T = 20;

h = waitbar(0,'load positive image files');
    positiveImages = loadImages(STR_POSITIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    p_i = ConvertToHog(positiveImages);
close(h);

h = waitbar(0,'load negative image files');
    negativeImages = loadImages(STR_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_TRAIN_IMAGE,1);
    n_i = ConvertToHog(negativeImages);
close(h);

h = waitbar(0,'load *testing* negative image files');
    testingNegativeImages = loadImages(STR_TESTING_NEGATIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_n_i = ConvertToHog(testingNegativeImages);
close(h);

h = waitbar(0,'load *tesitng* positive image files');
    testingPositiveImages = loadImages(STR_TESTING_POSITIVE_IMAGE_FORMAT,1,INT_MAX_VAILIDATION_IMAGE,1);
    t_p_i = ConvertToHog(testingPositiveImages);
close(h);

[V_P,V_N ,T_P,T_N ] = AdaConvertRidge(n_i, p_i, t_n_i, t_p_i);
trainVectorP = Convert(T_P)';
trainVectorN = Convert(T_N)';
validateVectorP  = Convert(V_P)';
validateVectorN  = Convert(V_N)';
%n = Convert(

display('training by adaboost ')
    [errorRate,tParameter, trainingClazz, validationClazz] = AdaTrainner(vertcat(trainVectorP,trainVectorN),vertcat(validateVectorP,validateVectorN),{INT_POSITIVE_CLASS,INT_NEGATIVE_CLASS},INT_MAX_T);
display('done.')

figure;
plot(tParameter,errorRate,'*');
title('T vs Error rate,  using Ridge-regression instead of SVM for �weak� classification ');
legend('y = train error rate','y = validation error rate')
xlabel(sprintf(' %d <= T <=  %d',1,INT_MAX_T));
ylabel('Error rates'); % y-axis label
display('classify the positive images...');
 %   positive_clazzes = svmclassify(trained,t_p);
display('done.');

%A = Reader('VldN1.pgm');
