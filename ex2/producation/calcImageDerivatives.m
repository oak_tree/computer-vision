function [X,Y] = calcImageDerivatives(img)
% calcImageDerivatives -
  filter = [ -1 0 1];
  X = conv2(img, filter ,'same');
  Y = conv2(img, filter' ,'same');
end

