function [H] = HOG(I, cell_x, cell_y, B)
% HOG 
% The function applies HOG measurements to a grayscale image.
% input:
% I: m-by-n grayscale image.
% cell_x: Positive integer holding the width of a HOG cell.
% cell_y: Positive integer holding the height of a HOG cell.
% B: Positive integer holding the number of HOG bins.
     
%output 
%H: (floor(m/cell_y)-1)-by-(floor(n/cell_x)-1) cell-array with entries corresponding to HOG
%blocks. Each entry holds a B-element row vector representing the oriented gradient
%histogram of the respective block. 
D = double(I);
[Dx, Dy ] =calcImageDerivatives(D);
[magnitude, angle ] =calcImageGradient(Dx,Dy);

m = size(I,1);
n = size(I,2);

H = cell((floor(m/cell_y)-1),(floor(n/cell_x)-1));
for i=1:(floor(m/cell_y)-1)
    for j=1:(floor(n/cell_x)-1)
        beans = zeros(1,B);
        [horizontalCordinates, vercticalCordinates]=  meshgrid((i-1)*cell_y+1:(i+1)*cell_y,(j-1)*cell_x+1:(j+1)*cell_x);
        cellAngle = angle(horizontalCordinates,vercticalCordinates);
        cellMagnitude = magnitude(horizontalCordinates,vercticalCordinates);
       for b=1:B
            minAngle = (-pi + 2*pi*(b-1)/B);
            maxAngle = (-pi + 2*pi*b/B);
            mag = cellMagnitude( cellAngle > minAngle & cellAngle < maxAngle);
            beans(b)= sum(mag(:));
        end
        
        H{i,j} = beans;
    end
end

H = cellfun(@(x) x./norm(x),H,'UniformOutput', false);
    
end

