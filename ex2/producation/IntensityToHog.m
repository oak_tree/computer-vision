function hogCells = IntensityToHog( cells )
%READER Convert cell to vectors in R^d but instead of just doing it on
%pixel intesity use Hog descriptore to achieve that
%   Detailed explanation goes here

%multi matrix
numberOfCells = size(cells,1);
hogCells = cells;
%D = reshape(cell2mat(cells), [1,4000,size(cells,1) ]);

cell_x = 10;
cell_y = 9;
B = 8;
D = cell(numberOfCells);
for i=1:numberOfCells
   hogCells{i} = cell2mat(HOG(cells{i},cell_x, cell_y,B)) ;
end

%D = reshape(cell2mat(cells), [size(cells,1),4000 ]);


end

