function [w] = RidgeTrain(X,y, lambda)
% RidgeTrain - The function implements inhomogeneous Ridge-regression training
%Inputs
% X: m-by-d real matrix where each row holds a training vector.
% y: m-element real column vector where entry i holds the label of row i in X.
% lambda: Positive scalar � regularization parameter lambda .

%Outputs
% w: d+1-element column vector representing the trained regressor (see description
%below). 

% solution w = (X^tX + 2*\lamdaI)^-1\cdot X^t\cdot t

X(isnan(X))=0;
%create column vectors of 1's
big_X  = [X ones(size(X,1),1)];
m = big_X' *big_X;
firstPart =  m+ 2*lambda * eye(size(m));
w = firstPart^-1 * big_X' * y;
% make sure to return column vector
w = w' ;


end

