function hogCells = ConvertToHog( cells )
%READER Convert cell to vectors in R^d but instead of just doing it on
%pixel intesity use Hog descriptore to achieve that
%   Detailed explanation goes here

%multi matrix
numberOfCells = size(cells,1);
hogCells = cells;
%D = reshape(cell2mat(cells), [1,4000,size(cells,1) ]);

cell_x = 10;
cell_y = 10;
B = 9;

t = waitbar(0,'calculate hog of images');
    for i=1:numberOfCells
       hogCells{i} = HOG(cells{i},cell_x, cell_y,B) ;
       waitbar(i / numberOfCells);
    end
close(t);

%D = reshape(cell2mat(cells), [size(cells,1),4000 ]);


end

