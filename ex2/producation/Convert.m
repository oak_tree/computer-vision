function D = Convert( cells )
%READER Convert cell to vectors in R^d
%   Detailed explanation goes here

%multi matrix
numberOfCells = size(cells,1)*size(cells ,2);
imageSize =  size(cells{1});
%D = reshape(cell2mat(cells), [1,4000,size(cells,1) ]);
I = cell2mat(cells);
D = reshape(I',[imageSize(2),imageSize(1),numberOfCells]);
D = reshape(D,[size(D,1)*size(D,2),numberOfCells]);
D = double(D)';
%D = reshape(cell2mat(cells), [size(cells,1),4000 ]);


end

