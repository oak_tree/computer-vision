function [images] = loadImages(baseImageName, firstFileIndex, lastFileIndex, jump)
% loadImages - load image files. allow to set prefix, index of start, index
% of last and the jump that should be consider 
% baseImageName - prefix i.e "ABA_A
% firstFileIndex - the first index to search for i.e 1 this will result
% that first file will be ABA_A1
% lastFileIndex - maximum of file index to search for 
% jump - allow to set jump between the indexes
% return cells of all the loaded images
     

A = cell(ceil((lastFileIndex - firstFileIndex)/jump),1);

counter = 1;

for i=firstFileIndex:jump:lastFileIndex
    currentFileName =  sprintf(baseImageName, i) ;
    currentImageRgb = Reader(currentFileName);
     A{counter} = currentImageRgb;
     counter = counter + 1;
       waitbar((i*jump / lastFileIndex));
end

images = A;

end

