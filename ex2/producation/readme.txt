late submission: The grader gave me extesntion till 21\5\2015 due  due to Miluim. 
---------------
PART 1
---------------
RunnerPart1.m		- Runner of part 1
Reader.m		- Simple interface for reading. used in loadImages.
loadImages		- load files into cells. 							- [used in other parts as well]
Convert.m		- convert array of cells into [M x d] matrix.					- [used in other parts as well]
Trainer.m		- Train with svm and validate it.						- [used in other parts as well]
Validation.m		- Calculate the learing error.							- [used in other parts as well]
ValidationTester.m  	- Simple Tester for Validation function.
---------------
PART 2
---------------

RidgeTrain.m		- Ridge trainer.								- [used in other parts as well]


---------------
PART 3
---------------
RidgeRunnerPart3.m	- Runner of part 3
RidgeTrainner.m		- Feed the Ridge Trainner.m							- [used in other parts as well]

---------------
PART 4
---------------

HOG.m			- Hog trainer									- [used in other parts as well]
calcImageDerivatives.m 	- calculate image derivatives
calcImageGradient.m 	- calculate image gradient

---------------
PART 5
---------------

HogRunnerPart5.m	- Part 5 Runner
IntensityToHog.m 	- Convert intensity ( array cell of image intensity) into HOG feature domain 	- [used in other parts as well]


---------------
PART 6
---------------

RidgeRunnerPart6.m	- Part 6 Runner.


---------------
PART 7
---------------
AdaRunnerPart7.m	- Part 7 Runner.
ConvertToHog.m		- Convert array cell of images into array cell of hog cells (of each image).	- [used in other parts as well]
AdaConvert.m		- Convert Array cell of HOG cells into features domain using svm.		
AdaTrainner.m		- Runner of AdaBoost. 


---------------
PART 8
---------------
AdaRunnerPart8.m	- Part 8 Runner.
AdaConvertRidge.m	- Convert Array cell of HOG cells into features domain using ridge regression.


REPORT:
report.pdf

supplied:
adaboost.m





