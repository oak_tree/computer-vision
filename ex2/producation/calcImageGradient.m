function [magnitude,angle] = calcImageGradient(Dx,Dy)
% calcImageGradient -
   
  magnitude = sqrt(Dx.^2 + Dy.^2);
  angle = atan(Dy./Dx);
  %make sure no NaN in the result matrix
  angle(isnan(angle))=0; 
end

