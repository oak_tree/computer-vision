function [errorRate, tParameter, trainingClazz, validationClazz] = AdaTrainner(trainVector,validateVector, clazzes, maxT )
%READER Summary of this function goes here
%   Detailed explanation goes here

trained = double(trainVector);
validate = double(validateVector);

positiveClass  =  clazzes{1,1};
negativeClass =  clazzes{1,2};

y = zeros(size(trained,1),1);
y(1:size(trained,1)/2,1) = positiveClass;
y(size(trained,1)/2+1:end,1) = negativeClass;


expectedResultValidation = zeros(size(validateVector,1),1);
expectedResultValidation(1:size(validateVector,1)/2,1) = positiveClass;
expectedResultValidation(size(validateVector,1)/2+1:end,1) = negativeClass;


X = trained;
X_ALL = vertcat(trained,validate);
expected_x_all = vertcat(y,expectedResultValidation);
i=1;
for T = 1:maxT
  
    display('================================');
    display(sprintf('train by adabost with lamda T=%d',T));
        
    [t_est,h] = adaboost('train',X,y,T);
    [all_est] = adaboost('apply',X_ALL,h);
    
    
    %trained = Validation(t_est,y);
        
    trained = Validation(y, all_est(1:size(X,1)));
    trainingClazz(i,:) = all_est(1:size(X,1));
    validated = Validation(expectedResultValidation, all_est(size(X,1)+1:end));
    
    validationClazz(i,:) = all_est(size(X,1)+1:end);
    errorRate(i,1) = trained;
   
    errorRate(i,2) = validated;
    %V = Validation(negativeValidation,expectedResultNegativeValidation,1);
    tParameter(i) = T;

    
    i = i + 1 ;
end

    

end

