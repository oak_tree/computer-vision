function V = Validation(values, expectedValues)
%READER Convert cell to vectors in R^d
%   Detailed explanation goes here

numberOfImages = max(size(values));
if (iscellstr(values))
    numberOfMisclassified=     sum(~strcmp(values,expectedValues));

else 
values(find(isnan(values))) = 0; % make to remove NaN values
numberOfMisclassified = ~~(values - expectedValues);
numberOfMisclassified = sum(numberOfMisclassified(:));
end 
V = numberOfMisclassified / numberOfImages;
end

