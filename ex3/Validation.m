function V = Validation(values, expectedValues)
%Validation calculate the average of distinguish values between "values" &
%"expectedValues
%   In: 
%      values, expectedValues - should be the same time. Both can be
%      cell-array or vectors or matrix, but have to the same to make the
%      function work

numberOfImages = max(size(values));
if (iscellstr(values))
    numberOfMisclassified=     sum(~strcmp(values,expectedValues));

else 
values(find(isnan(values))) = 0; % make to remove NaN values
numberOfMisclassified = ~~(values - expectedValues);
numberOfMisclassified = sum(numberOfMisclassified(:));
end 
V = numberOfMisclassified / numberOfImages;
end

