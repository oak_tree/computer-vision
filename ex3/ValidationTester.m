%READER Convert cell to vectors in R^d
%   Detailed explanation goes here

A = randi(10,[1,5]);
B = randi(10,[1,5]);
Validation(A ,B);

 A = [9     3    10     4     2];
 B =  [3     7     5     4     9];
 V = Validation(A ,B);
 if ( V ~=  (4/5)) 
     display('error on validation function with A = ');
     display(A);
     display(['and  B = ']);
     display(B);
 else 
     display(['validation function works ok with A = ']);
     display(A);
     display(['and  B = ']);
     display(B);
 end
 
 A = [cellstr('home')    cellstr( 'dog')    cellstr('sea')    cellstr('apple') ];
 B = [cellstr('home')    cellstr( 'cat')    cellstr('beatch')    cellstr('apple') ];
 V = Validation(A ,B);
 if ( V ~=  (2/4)) 
     display('error on validation function with A = ');
     display(A);
     display(['and  B = ']);
     display(B);
 else 
     display(['validation function works ok with A = ']);
     display(A);
     display(['and  B = ']);
     display(B);
 end
 
 
 A = [cellstr('home')    cellstr( 'dog')    cellstr('sea')    cellstr('apple') cellstr('banana') ];
 B = [cellstr('home')    cellstr( 'cat')    cellstr('beatch')    cellstr('apple') cellstr('banana') ];
  V = Validation(A ,B);
 if ( V ~=  (2/5)) 
     display('error on validation function with A = ');
     display(A);
     display(['and  B = ']);
     display(B);
 else 
     display(['validation function works ok with A = ']);
     display(A);
     display(['and  B = ']);
     display(B);
 end
 
  A = [cellstr('home')    cellstr( 'dog')    cellstr('sea')    cellstr('apple') cellstr('banana') ];
 B = [cellstr('home')    cellstr( 'cat')    cellstr('beatch')    cellstr('apple') cellstr('banana') ];
 V = Validation(A ,B);
 if ( V ~=  (3/5)) 
     
     display(['validation function works ok with A = ']);
     display(A);
     display(['and  B = ']);
     display(B);
 else  
     display('error on validation function with A = ');
     display(A);
     display(['and  B = ']);
     display(B);
 
 end
 
 
 A = [9     NaN    10     4     2];
 B =  [3     7     5     4     9];
 V = Validation(A ,B);
 if ( V ~=  (4/5)) 
     display('error on validation function with A = ');
     display(A);
     display(['and  B = ']);
     display(B);
 else 
     display(['validation function works ok with A = ']);
     display(A);
     display(['and  B = ']);
     display(B);
 end
