function [ W ] = letter_hinge_SGD(X, Y, lambda, T )
%LETTER_HINGE_SGD  learns a letterclassifier
%via SGD-minimization of the regularized hinge-loss.
%
% Inputs
% X: m-by-d real matrix where each row holds the feature measurements of a different
% handwritten letter.
% Y: m-element vector of integers between 1 and k, where each entry holds the label of
% the respective row in X.
% lambda: Non-negative scalar. Regularization parameter.
% T: Positive integer. Number of SGD iterations to carry out.
%
% Outputs
% W: k-by-d real matrix representing the letter-classifier (rows correspond to classes,
% columns correspond to feature measurements) learned via SGD-minimization of
% regularized hinge-loss. 



end

