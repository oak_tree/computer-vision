function [ Y ] = classify_letters( X,W )
%CLASSIFY_LETTERS classify letter by givin classifier
% The function uses the letter-classifier represented by W to predict the labels of the rows in X.
%This is done with the following linear classification rule:
% \hat{y}(x) = argmax<W_r,X> for r \in [1..k]


%run w_r,x for each x and r, choose the max for each one of them

X_T = X';
all_possible_mul = W * X_T;
% search for max in each column. this will give the right classifer
[~,Y] = max(all_possible_mul);

end

