KMeans.m - the requested function
nextC.m - calculate the next clustering centers (used in KMeans.m)
nextW.m  - calcuate the next W (used in KMeans.m)
Convert.m - the T_mui function
EX1-2Runner.m  - a runnner( script) to run q1/q2 tasks

SpectralCluster.m - the requested function
GenerateAfinity.m - generate the affinitiy matrix 
Scale.m - make the code more readble - just scale the image with 1/8 
EX3-4Runner.m - a runnner( script) to run q3/q4 tasks

GenerateAffinityAB.m - special generate function to generate affinity by size demands
NystromNCuts.m - supplied function
EX5-Runner.m - a runnner( script) to run q5 tasks

PlotLabels.m - helper to plots the label - use matlab function to make random color picker
Reader.m - make the code more readble  - just read an image file


As state in the forum: 

Memory problem on question 5 with n=400....out of memory