function [ c_next ] = nextC(w,X,C )
    

    c_temp = C;
    
    for j=1:size(C,2)
        temp = X(:,w(:,j)==1);
        c_temp(:,j) = sum(temp,2) / size(temp,2);
    end     
    c_next = c_temp;

end


