function [C, labels] = KMeans(X, k, max_iter, change_thresh, num_runs)
% The function applies the k-means clustering algorithm on the given data points (in X) num_runs
% times, and returns the clustering which produced the best (lowest) objective. Each k-means run
% is initialized by randomly choosing k different data points and setting them as centroids. A run
% terminates when any of the following two conditions is met:
%     1. max_iter iterations have been carried out.
%     2. The change in the objective between two consecutive iterations is less than
% change_thresh.


%iterates
d = size(X,1); 

%initialize:
C = rand(d,k);
[w] = nextW(X,C);


%steps

for i=1:num_runs
    
    
    if i == max_iter
        break;
    end
    
    
    old_c = C;
    c = nextC(w,X,C);
    
    %calcuate min change thresh
    subC = old_c-c;
    subC = sum(subC.^2);
    if max(subC) <  change_thresh
        break;
    end 
    
    
    w = nextW(X,C);
    
end

C = c;
[ rows , ~] = find(w');
labels = rows';

end


