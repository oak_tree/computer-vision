function converted = GenerateAffinity(img, sigma_c, sigma_s)
%GenerateAffinity generate an affine matrix for the image img



disp('converting it to lab...')
    cform = makecform('srgb2lab');
    A = applycform(img,cform);
disp('done')

disp('calculate pixels positions...')
    [ rows , columns] = meshgrid(1:size(A,2),1:size(A,1));
    % save each pixel position
    A(:,:,4)=columns;
    A(:,:,5)=rows;
disp('done')

disp('convert matrix to [d x m] vector')
    % convert image new matrix to big vector M
    X = double(reshape(A,[size(A,2)* size(A,1),size(A,3)]));
    X=X';
disp('done')

disp('calcuating norm....')
    %duplicate  x vectors for each cluster
    x_one = repmat(X,1,1,size(X,2));
    %switch between two other dimenstion 
    x_two = permute(x_one,[1 3 2 ]) ; 

    norm = x_one-x_two;
    norm = norm.^2;

    lab_norm = norm(1:3,:,:);
    cordinate_norm = norm(4:5,:,:);

    lab_norm = sum(lab_norm)*(1/(2*sigma_c^2));
    cordinate_norm = sum(cordinate_norm)*(1/(2*sigma_s^2));


    converted = squeeze(exp(-lab_norm-cordinate_norm));
disp('done')


end

