function [ w_next ] = nextW(X,C )

    w = zeros(size(X,2),size(C,2));

    %duplicate  x vectors for each cluster
    x_s = repmat(X,1,1,size(C,2));
    
    %spread cluster centers on the 3rd dimnestion
    c_s = reshape(C,1,[], size(C,2));
    c_s = permute(c_s, [2 1 3]);
    
    %duplicate clusters. make sure each X_i vectors has all the cluster
    %centeers
    c_s = repmat(c_s,1,size(X,2),1);
    
    %calculat the norm of each x_i against all cluster centers
    norm = (x_s-c_s).^2;
    norm = sum(norm);
    
    %find the closet center for each x_i
    D = reshape(norm,size(X,2),size(C,2));
    D = D' ; % min works on columns.
    [~,I] = min(D);
    %col = 1:size(D,2);
    
    indexes = sub2ind(size(D),I,1:size(D,2));
    %mark the closet center for each x_i
    w_transpose = w';
    %w_transpose(col,I) = 1;
    w_transpose(indexes) = 1;
    w_next = w_transpose';

end


