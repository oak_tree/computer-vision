
d = 5; 
k = 4;
max_iter = 100;
change_thresh = 1.75;
num_runs = 50; % what is the difference to max_iter?
mui=0.3;

I = Reader('I1.jpg');
A = Convert(I,mui);
X = double(reshape(A,[size(A,2)* size(A,1),size(A,3)]));
X=X';
%X = double(reshape(A,[size(A,3) , size(A,1)*size(A,2)]));
[C, labels] = KMeans(X, k,max_iter , change_thresh, num_runs);
PlotLabels(labels,I);

