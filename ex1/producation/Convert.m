function converted = Convert( img, miu )
%READER Summary of this function goes here
%   Detailed explanation goes here

if ~exist('miu', 'var') 
    miu = 1;
end


cform = makecform('srgb2lab');
A = applycform(img,cform);
[ rows , columns] = meshgrid(1:size(A,2),1:size(A,1));
columns = columns.*miu;
rows = rows.*miu;


converted =  A ; 
converted(:,:,4)=columns;
converted(:,:,5)=rows;



end

