function [ w_next ] = minNorm(X,C )

w = zeros(size(X,2),size(C,2));


    for j=1:size(C,2)
        C_J = C(:,j);
        N = X-diag(C_J')*ones(size(X));
        N = N.^2;
        N = sum(N) % over d 
        [~,I] = min(N)
        w(I,j) = 1;
    end     
    w_next = w 

end


