function [ argmins ] = ArgminOnSum( w,X,C , condition);
%ARGMINONSUM this calculate the argmin of 1/2 * sum on i * sum of j * w_i,j
%|| X_i - c_j|| ^2


% note that one can pass X, C in different order to recieve argmin on C
% condition will be HOLD ON X


%D = bsxfun(@minus, X, C');
a = [x 3 4];      %first take two vector a and b of any size
b = [5 6 5 7];
m = size(a);      % Then Calculate the size of the vectors
n = size(b);  
r1 = a'*ones(n);  % replicate the vector a and b one can use **repmat** here for replication  
r2 = ones(m)'*b;  % like **repmat(a',n)  &  repmat(b,m(end),1)**
D=r1-r2;
R = D .* w * 0.5;
f = sum(sum(R));
subs(f,x,0:1e-3:10 );
end

