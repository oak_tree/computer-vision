function labels = SpectralCluster(W, k, algo)


    
disp('calculating D...')
    D = diag (W*ones(max(size(W)),1));

disp('deciding which algo to use....')
    if strcmp(algo, 'rc')
        %+1 is like +I in matlab
        W_tag = W-D +1;
        disp('using rc');
    elseif  strcmp(algo,'nc')
        D_S = D^-1;
        D_S = D_S^0.5;
        W_tag = D_S * W * D_S;
        disp('using nc');
    end
disp('done..')


disp('solving argmax...')
    [V,D] = eig(W_tag);
    [~,permutation]=sort(diag(D),'descend');
    D=D(permutation,permutation);
    V_star=V(:,permutation);
disp('done..')

disp('normalizing V*...')
    V1          = V_star(:,1:k);
    Vnormalized = V1./repmat(sqrt(sum(V1.^2,2)),1,k);
disp('done..')

disp('running k-means...')
    [~,labels]  = KMeans(Vnormalized',k,25,0,5);
disp('done..')
end