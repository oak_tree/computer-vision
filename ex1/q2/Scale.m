function [ scaled ] = Scale( img )
%Scale the given image in factor of 1/8

disp('resize image in factor of 1/8...')
    %scale the image  - othersize results will be huge matrix!
    scaled = imresize(img,1/8);
disp('done')



end

