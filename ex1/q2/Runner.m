
k = 4;
algo = 'nc';
sigma_s = 10;
sigma_c = 20;

I = Scale(Reader('I1.jpg'));
W = GenerateAffinity(I,sigma_c, sigma_s);
labels = SpectralCluster(W,k,algo);
PlotLabels(labels,I);
I = Scale(Reader('I2.jpg'));
W = GenerateAffinity(I,sigma_c, sigma_s);
labels = SpectralCluster(W,k,algo);
PlotLabels(labels,I);
I = Scale(Reader('I3.jpg'));
W = GenerateAffinity(I,sigma_c, sigma_s);
labels = SpectralCluster(W,k,algo);
PlotLabels(labels,I);
