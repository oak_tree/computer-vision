
d = 5; 
m = 321*481;
m = 7;
X = size(d,m);
k = 3;
max_iter = 100;
change_thresh = 1.6;

num_runs = 50; % what is the difference to max_iter?

X = rand(d,m);
C = rand(d,k);
[w] = nextW(X,C);
[C] = nextC(w,X,C);


X = rand(d,m);
[C, labels] = KMeans(X, k,max_iter , change_thresh, num_runs);



%testing new C and X should be the same 
%[w] = nextW(X,X);
%[C] = nextC(w,X,X);

mui=0.3;

I = Reader('I1.jpg');
A = Convert(I,mui);
X = double(reshape(A,[size(A,2)* size(A,1),size(A,3)]));
X=X';
%X = double(reshape(A,[size(A,3) , size(A,1)*size(A,2)]));
[C, labels] = KMeans(X, k,max_iter , change_thresh, num_runs);
PlotLabels(labels,I);

I = Reader('I2.jpg');
A = Convert(I,mui);
X = double(reshape(A,[size(A,2)* size(A,1),size(A,3)]));
X=X';
%X = double(reshape(A,[size(A,3) , size(A,1)*size(A,2)]));
[C, labels] = KMeans(X, k,max_iter , change_thresh, num_runs);
PlotLabels(labels,I);

I = Reader('I3.jpg');
A = Convert(I,mui);
X = double(reshape(A,[size(A,2)* size(A,1),size(A,3)]));
X=X';
%X = double(reshape(A,[size(A,3) , size(A,1)*size(A,2)]));
[C, labels] = KMeans(X, k,max_iter , change_thresh, num_runs);
PlotLabels(labels,I);
