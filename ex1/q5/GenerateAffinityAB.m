function [A, B] = GenerateAffinityAB(img,size_A, sigma_c, sigma_s)
%GenerateAffinity generate an affine matrix for the image img



%

disp('converting rgb 2 lab...')
    cform = makecform('srgb2lab');
    labImg = applycform(img,cform);
disp('done.')

disp('calculating pixels positions..')
    [ rows , columns] = meshgrid(1:size(labImg,2),1:size(labImg,1));

    % save each pixel position
    labImg(:,:,4)=columns;
    labImg(:,:,5)=rows;
disp('done.')

disp('calc one big vector from img')
    % convert image new matrix to big vector M
    X = double(reshape(labImg,[size(labImg,2)* size(labImg,1),size(labImg,3)]));
    X=X';
disp('done.')


disp('calculate A...')
    temp_A = X(:,1:size_A);

    %duplicate  x vectors for each cluster
    A_one = repmat(temp_A,1,1,size(temp_A,2));
  
    %instead of doing permute on A_one. first do permute on temp_A and then
    %repmat it
    A_two = permute(temp_A,[1 3 2 ]) ; 
    A_two = repmat(A_two,1,size(temp_A,2),1);
    %switch between two other dimenstion 
    %A_two = permute(A_one,[1 3 2 ]) ; 

    norm = A_one-A_two;
    norm = norm.^2;


    lab_norm = norm(1:3,:,:);
    cordinate_norm = norm(4:5,:,:);

    lab_norm = sum(lab_norm)*(1/(2*sigma_c^2));
    cordinate_norm = sum(cordinate_norm)*(1/(2*sigma_s^2));


    A = squeeze(exp(-lab_norm-cordinate_norm));
disp('done.')

disp('calculate B...')
    B = X(:,size_A+1:end);

    %duplicate  x vectors for each cluster
    B_one = repmat(temp_A,1,1,size(B,2));
    %switch between two other dimenstion 
    %B_two = repmat(B,1,1,size(temp_A,2));
    %spread cluster centers on the 3rd dimnestion
    %B_two = permute(B_two, [1 3 2]);
    
    
    
    %instead of doing permute on A_one. first do permute on temp_A and then
    %repmat it
    B_two = permute(B, [1 3 2]);
    B_two = repmat(B_two,1,size(temp_A,2),1);


    %calculate norm
    norm = B_one-B_two;
    norm = norm.^2;

    lab_norm = norm(1:3,:,:);
    cordinate_norm = norm(4:5,:,:);

    lab_norm = sum(lab_norm)*(1/(2*sigma_c^2));
    cordinate_norm = sum(cordinate_norm)*(1/(2*sigma_s^2));


    B = squeeze(exp(-lab_norm-cordinate_norm));

disp('done.')


end

