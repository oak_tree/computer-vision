k = 3;
sigma_s = 100;
sigma_c = 1000;
size_A = 50;
I = Reader('I1.jpg');
[A,B] = GenerateAffinityAB(I,size_A,sigma_s,sigma_c);
labels = NystromNCuts(A,B,k);
PlotLabels(labels,I);
I = Reader('I2.jpg');
[A,B] = GenerateAffinityAB(I,size_A,sigma_s,sigma_c);
labels = NystromNCuts(A,B,k);
PlotLabels(labels,I);

I = Reader('I3.jpg');
[A,B] = GenerateAffinityAB(I,size_A,sigma_s,sigma_c);
labels = NystromNCuts(A,B,k);
PlotLabels(labels,I);
