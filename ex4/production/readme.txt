---------------
PART 1
---------------
batch_size_script.m  

---------------
PART 2
---------------
momentuim_script.m

---------------
PART 3
---------------
lambda_script.m

---------------
PART 4
---------------
part4_runner.m

---------------
PART 5
---------------
part5_runner.m


---------------
PART 6
---------------
best_small_script.m


---------------
PART 7
---------------
small_large_script.m
bigtrain.m - helper for training big network (this is the difference comparing train.m)

---------------
PART 8
---------------
best_script.m
part8train.m - helper to train custom network architecture


---------------
Shared utils
---------------
TopTen.m - Get top ten result from table
averageCellArray.m - average cell array result 
randomArrayPosition.m - get random position in an array
run_all.m - run all 1 to 5 scripts
train.m - helper for part 1 to part 5 to run the network 

---------------
REPORT:
---------------
report.pdf

-----------
supplied:
-----------
ConvNet.m
visualizeNetwork.m
lossClass.m
dataClass.m
data_disp_script.m
demoMNIST.m
template_script.m






