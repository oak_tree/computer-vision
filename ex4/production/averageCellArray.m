function [ meanArray ] = averageCellArray( cellArray )
%AVERAGECELLARRAY Calculate the average of cell arary
%   Detailed explanation goes here

    dim = ndims(cellArray{1});          %# Get the number of dimensions for your arrays
    M = cat(dim+1,cellArray{:});        %# Convert to a (dim+1)-dimensional matrix
    meanArray = mean(M,dim+1);          %# Get the mean across arrays
end

