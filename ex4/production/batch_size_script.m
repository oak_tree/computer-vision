disp('batch_size_script');    
batchSizes = [ 1,2,4,8,20];
    trainingRates= [0.5,0.1,0.05];

clear part1_train_err_itt;
showFigure=false;
for k= 1:5
    disp (['current loop position: k=', num2str(k)])
    
    testFlag = false;

    for i = 1:numel(batchSizes)
        for j = 1:numel(trainingRates)
            disp (['current loop position: i=', num2str(i) ,',j=', num2str(j)])
            batch_size    = batchSizes(i);
            conv_kernel   = 5;
            conv_stride   = 1;
            conv_channels = 5;
            pool_kernel   = 2;
            pool_stride   = 2;
            % Optimization parameters

            T      = 1e4/batch_size;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
            mu     = single(0);   % Momentum variable
            lambda = single(0);   % Regularization constant
            training_rate=trainingRates(j);
            eta    = @(t)(training_rate); % Learning rate - eta(t) returns the value for iteration t
            affine_weights= 'Xavier';
            [single_run_error,~]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
            part1_train_err_itt{k}(i,j,:) =[batch_size,training_rate, single_run_error];
                        
        end
    end
end

%calc average 
%part1_train_err(:,:,:)=mean(iit);
clear part1_train_err;
part1_train_err = averageCellArray(part1_train_err_itt);
figure;
bar(batchSizes, part1_train_err(:,:,3),'DisplayName','train_err')
title('Part 1 - Batch size vs Learning rate vs Train accuracy');
ylabel('Accuracy');
xlabel('Batch size');
N=trainingRates;
legend(strcat('\mu=',strtrim(cellstr(num2str(N(:))))));
% part1_graph_data = reshape(part1_train_err(15,3));
% figure(1)
% plot3(part1_graph_data(:,1),part1_graph_data(:,2),part1_graph_data(:,3),'.')
% mesh(batchSizes,trainingRates,part1_train_err(:,:,3)')
% figure(2)
% plot(part1_train_err(1,:,2),part1_train_err(1,:,3),'.r')
% figure(3)
% plot(part1_train_err(:,1,1),part1_train_err(:,1,3),'.b')
