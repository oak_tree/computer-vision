disp('best script');

%% Helper parameters

testFlag = true;
showFigure=false;
%% Hyper network parameters



    % for each parameter calc the average on 5 runs
    batch_size    = 10;
    conv_kernel1   = 5;
    conv_kernel2   = 3;
    conv_kernel3   = 3;
    
    conv_stride   = 1;
    conv_channels1 = 95;
    conv_channels2 = 48;
    conv_channels3 = 48;
    
    pool_kernel   = 2;
    pool_stride   = 2;
    k2 = 1000;
    % Optimization parameters
    
    T      = 1e4;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
    mu     = 0.9; % default = single(0);   % Momentum variable
    lambda = 0.0005;   % Regularization constant
    learning_rate=0.005;
    eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
    affine_weights= 'Xavier';
% DESCRIPTIVE TEXT
%% Regular network structure
% DESCRIPTIVE TEXT

clear part8_A_train_err_itt;
clear part8_A_train_err;


     part8_A_train_err_itt = cell(5);
    for k = 1:5
        disp(k)
        [single_run_error,single_test_error]= part8train(testFlag,batch_size,conv_kernel1,conv_kernel2,conv_kernel3,conv_stride,conv_channels1,conv_channels2,conv_channels3,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,k2,showFigure);
        part8_A_train_err_itt{k}= [single_run_error,single_test_error];
    end
    
    part8_A_train_err  = averageCellArray(part8_A_train_err_itt);

save('part8_train_err_run.mat','part8_A_train_err');

