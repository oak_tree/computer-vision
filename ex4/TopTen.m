function [ top ] = TopTen( matrix,columnNumber )
%TopTen Get the top ten rows of a matrix
%   columnNumber, the column number to sort by

x = matrix;
[~,idx] = sort(x(:,columnNumber),'descend');
top = x(idx(1:10),:);

end

