disp('momentuim_script');
clear part2_train_err_itt;
%batchSizes = [ 1,2,4,8,20];

momentums= [0.9,0.95,0.99];
learningRates= [5e-2, 1e-2, 5e-3, 1e-3, 5e-4, 1e-4];

testFlag = false;
showFigure = false;
for k = 1:5
    for i = 1:numel(momentums)
        for j = 1:numel(learningRates)
            disp [i, ',', j]
            batch_size    = 1;
            conv_kernel   = 5;
            conv_stride   = 1;
            conv_channels = 5;
            pool_kernel   = 2;
            pool_stride   = 2;
            % Optimization parameters
            
            T      = 1e4/batch_size;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
            mu     = momentums(i); % default = single(0);   % Momentum variable
            lambda = single(0);   % Regularization constant
            learning_rate=learningRates(j);
            eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
            affine_weights= 'Xavier';
            [single_run_error,~]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
            part2_train_err_itt{k}(i,j,:) =[mu,learning_rate, single_run_error];
        end
    end
end

clear part2_train_err;
part2_train_err = averageCellArray(part2_train_err_itt);


figure;
bar(momentums, part2_train_err(:,:,3),'DisplayName','train_err')
title('Part 2 - Momentum size vs Learning rate vs Train accuracy');
ylabel('Accuracy');
xlabel('Momentum');
N=learningRates;
legend(strcat('\eta=',strtrim(cellstr(num2str(N(:))))));
