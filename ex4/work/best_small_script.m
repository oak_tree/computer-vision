disp('best small script');
clear part6_train_err_itt;
clear part6_train_err;

batchSizes = [ 1,2,4,8,20];
momentums= [0.9,0.95,0.99];
learningRates= [5e-2, 1e-2, 5e-3, 1e-3, 5e-4, 1e-4,0.5,0.1,0.05];
lambdas=[0, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2 , 5e-2];
testFlag = true;
%run 500 different random cases in all above parameters...
showFigure=false;
for i = 1:398
    a = randomArrayPosition(batchSizes);
    b = randomArrayPosition(momentums);
    c = randomArrayPosition(lambdas);
    d = randomArrayPosition(learningRates);
    disp (['run number',i]);
    disp (['running  with the parameters:',a,b,c,d]);
    % for each parameter calc the average on 5 runs
    batch_size    = batchSizes(a);
    conv_kernel   = 5;
    conv_stride   = 1;
    conv_channels = 5;
    pool_kernel   = 2;
    pool_stride   = 2;
    % Optimization parameters
    
    T      = 1e4/batch_size;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
    mu     = momentums(b); % default = single(0);   % Momentum variable
    lambda = lambdas(c);   % Regularization constant
    learning_rate=learningRates(d);
    eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
    affine_weights= 'Xavier';
    
    part6_train_err_itt = cell(5);
    for k = 1:5
        disp(k)
        [single_run_error,single_test_error]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
        part6_train_err_itt{k}= [single_run_error,single_test_error];
    end
    
    part6_train_err(i,:) = [averageCellArray(part6_train_err_itt),batch_size,T,mu,lambda,learning_rate];
end
save('part6_train_err_run.mat','part6_train_err');
% get Top ten results
y=TopTen(part6_train_err,2);
array2table(y,'VariableNames',{'TrainAccuracy' 'TestAccuracy' 'BatchSize' 'NumberofIterations' 'Mu' 'Lambda' 'LearingRate'})
