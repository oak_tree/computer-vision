disp('best small script');

%% Helper parameters

testFlag = true;
showFigure=false;
%% Hyper network parameters



    % for each parameter calc the average on 5 runs
    batch_size    = 10;
    conv_kernel   = 5;
    conv_stride   = 1;
    conv_channels = 5;
    pool_kernel   = 2;
    pool_stride   = 2;
    % Optimization parameters
    
    T      = 1e4;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
    mu     = 0.9; % default = single(0);   % Momentum variable
    lambda = 0.0005;   % Regularization constant
    learning_rate=0.005;
    eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
    affine_weights= 'Xavier';
% DESCRIPTIVE TEXT
%% Regular network structure
% DESCRIPTIVE TEXT

clear part7_A_train_err_itt;
clear part7_A_train_err;


      part7_A_train_err_itt = cell(5);
    for k = 1:5
        disp(k)
        [single_run_error,single_test_error]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
        part7_A_train_err_itt{k}= [single_run_error,single_test_error];
    end
    
    part7_A_train_err  = averageCellArray(part7_A_train_err_itt);

save('part7_train_err_run.mat','part7_A_train_err');

%% Big network structure
% DESCRIPTIVE TEXT

conv_channels1=20;
conv_channels2=50;



clear part7_B_train_err_itt;
clear part7_B_train_err;

    part7_B_train_err_itt = cell(5);
    for k = 1:5
        disp(k)
        [single_run_error,single_test_error]= bigtrain(testFlag,batch_size,conv_kernel,conv_stride,conv_channels1,conv_channels2,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
        part7_B_train_err_itt{k}= [single_run_error,single_test_error];
    end
    
    part7_B_train_err  = averageCellArray(part7_B_train_err_itt);

save('part7_train_err_run.mat','part7_A_train_err','part7_B_train_err');