
        testFlag = true;

        batch_size    = 1;
        conv_kernel   = 5;
        conv_stride   = 1;
        conv_channels = 5;
        pool_kernel   = 2;
        pool_stride   = 2;
        % Optimization parameters

        T      = 1e4/batch_size;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
        mu     = single(0);   % Momentum variable
        lambda = single(0);   % Regularization constant
        learning_rate=0.05;
        eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
        affine_weights= 'Zeros';
        showFigure = false;
        [single_run_error,single_run_error]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
        part4_train_err =[learning_rate, single_run_error];

% part4_graph_data = reshape(part4_train_err(size(momentums,2),size(learningRates,2)));
% 
% figure(1)
% plot3(part4_graph_data(:,1),part4_graph_data(:,2),part4_graph_data(:,3),'.')
% mesh(batchSizes,trainingRates,part4_train_err(:,:,3)')
% figure(2)
% plot(part4_train_err(1,:,2),part4_train_err(1,:,3),'.r')
% figure(3)
% plot(part4_train_err(:,1,1),part4_train_err(:,1,3),'.b')
% 
