disp('lambda_script');
%batchSizes = [ 1,2,4,8,20];

%momentums= [0.9,0.95,0.99];
%learningRates= [5e-2, 1e-2, 5e-3, 1e-3, 5e-4, 1e-4];
lambdas=[0, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2 , 5e-2];

% note that in part 3 we are running also test and not only train error
testFlag = true;
clear part3_train_err_itt;
showFigure = false;
for k= 1:5
    disp (['current loop position: k=', num2str(k)]);
        
    for i = 1:numel(lambdas)
        disp (['current loop position: i=', num2str(i)]);
        batch_size    = 1;
        conv_kernel   = 5;
        conv_stride   = 1;
        conv_channels = 5;
        pool_kernel   = 2;
        pool_stride   = 2;
        % Optimization parameters
        
        T      = 1e4/batch_size;         % No. of iterations (10K x 1-batch = 10K examples passed through the network)
        mu     = single(0);   % Momentum variable
        lambda = lambdas(i);              % Regularization constant
        learning_rate=0.05;              % default
        eta    = @(t)(learning_rate); % Learning rate - eta(t) returns the value for iteration t
        affine_weights= 'Xavier';
        [single_run_train_error,single_run_test_error]= train(testFlag,batch_size,conv_kernel,conv_stride,conv_channels,pool_kernel,pool_stride,T,mu,lambda,eta,affine_weights,showFigure);
        part3_train_err_itt{k}(i,:) =[lambda,[single_run_train_error, single_run_test_error]];
        
    end
end

clear part3_train_err;
part3_train_err = averageCellArray(part3_train_err_itt);

figure;
plot(log(part3_train_err(:,1)),part3_train_err(:,2),'r','DisplayName','train_err')
hold
plot(log(part3_train_err(:,1)),part3_train_err(:,3),'b','DisplayName','train_err')
title('Part 3 - Lambda size vs Test Accuracy rate vs Train Accuracy');
ylabel('Accuracy');
xlabel('log(Lambda)');
N=learningRates;
legend('train accuracy','test accuracy');
