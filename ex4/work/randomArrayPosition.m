function [i]= randomArrayPosition(arr)
% randomArrayPosition get random index to access the array "arr"
% input: array arr
% output : integer 0<x<=length(array)
msize = numel(arr);
i = randperm(msize, 1);
end