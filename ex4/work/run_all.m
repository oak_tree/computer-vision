batch_size_script
save('part1-1-data.mat')

momentuim_script
save('part1-2-data.mat')

lambda_script
save('part1-3-data.mat')

for i=1:5
    part4_runner
    part4_train_err_itt{k}=part4_train_err;
end
clear part4_train_err;
part4_train_err=averageCellArray(part4_train_err_itt);
save('part1-4-data.mat')


clear part5_train_acc_itt;
clear part5_test_acc_itt;
for i=1:5
    part5_runner
    part5_train_acc_itt{k} = part5_train_acc;
    part5_test_acc_itt{k}= part5_train_acc;
end
clear part5_train_acc;
clear part5_test_acc;
part5_train_acc=averageCellArray(part5_train_acc_itt);
part5_test_acc=averageCellArray(part5_test_acc_itt);
save('part1-5-data.mat')
    
