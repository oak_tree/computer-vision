function [Norms, w_next ] = Copy_of_nextW( w,X,C )

w = zeros(size(X,2),size(C,2));
    for i=1:size(X,2)
        X_I = X(:,i);
        N = C-diag(X_I')*ones(size(C))
        N = N.^2;
        N = sum(N)
        [~,I] = min(N)
        w(i,I) = 1;
    end     
    w_next = w 

end


