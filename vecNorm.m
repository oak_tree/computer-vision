function [Norms, w_next ] = vecNorm( X,C )

%update w
m=size(X);      % Then Calculate the size of the vectors
n=size(C);
r1=X'*ones(n);  % replicate the vector a and b one can use **repmat** here for replication  
r2=ones(m)'*C;  % like **repmat(a',n)  &  repmat(b,m(end),1)**
Norms=r1-r2;
%Norms = Norms.^2;
Norms = Norms;

% this can be done with 
Norms == bsxfun(@minus, X', C);
%Norms == bsxfun(@minus, X', C).^2 % return 1 :)

end


